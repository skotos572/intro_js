//tipo de datos

let cliente

console.log(typeof cliente)

//boolean
const descuento = true;

console.log(typeof descuento)

//nuemros
const nume1 = 20.30

const num2 = 2;
const num3 = -200;

console.log(typeof nume1)
console.log(typeof num2)
console.log(typeof num3)

//string

const alumno = "jesus"

console.log(typeof alumno)

//big decimals
const numeroBig = BigInt(123456654654365436345654364536435634.564)
console.log(typeof numeroBig)

//symbols (unicos y diferentes)
const simbolo = Symbol(1)

console.log(simbolo)

//Object

const numeros = [1,2,3]

console.log(typeof numeros)

