//unir dos objetos

const productos = {
    nombre: "Tablet",
    precio: 300,
    disponible: true
}

const cliente = {
    nombre: 'jesus',
    premium: true,
    precio: 50
}
//object assing une los dos objetos pero sobre escribe los valores identicos dejando solo uno
//const nuevoObjt = Object.assign(cliente, productos)

const nuevoObjt = {
    detalle: productos,
    usuario: cliente
}
console.table(nuevoObjt)