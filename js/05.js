

const productos = {
    nombre: "Tablet",
    precio: 300,
    disponible: true
}
//no permite reescribir , añadir o eliminar propiedades
//Object.freeze(productos)

// seal solo permite modificar el valor 
Object.seal(productos)

// reescribir valor

productos.nombre = "monitor curvo"

//Añadir un valor

productos.imagen = "imagen.jpg"

//eliminar

//delete productos.disponible

//elimina una propiedad del objeto original y genera un nuevo objeto sin este atributo
//const {disponible, ...productos2} = productos

console.log(productos)