// destructuring de 2 o mas objetos

const productos = {
    nombre: "Tablet",
    precio: 300,
    disponible: true
}

const cliente = {
    nombre: 'jesus',
    premium: true
}

console.log(productos.nombre)
console.log(cliente.nombre)

//se accede al nombre de cada objeto y se renombra
const {nombre: nombreProducto} = productos
const {nombre: nombreCliente} = cliente

console.log(nombreProducto)
console.log(nombreCliente)

