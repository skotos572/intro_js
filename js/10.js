
const tegnologia = ['vue','angular','reac','css']

//añadir elementos al final
tegnologia.push('git')

//añadir elementos al principio
tegnologia.unshift('jenkings')

//añadir atributos sin modificar el arreglo original
const nuevoArreglo = [...tegnologia]

nuevoArreglo.push('java')

//eliminar elemento del final del arreglo
nuevoArreglo.pop()

//eliminar al pricipio
nuevoArreglo.shift()

//eliminar por enmedio (psicion, numero de elementos a eliminar)
nuevoArreglo.splice(1,1)

console.table(tegnologia)
console.table(nuevoArreglo)