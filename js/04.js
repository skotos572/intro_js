//Objetos 

//const producto = "tablet"
//const precio = "300"
//const disponible = true

const productos = {
    nombre: "Tablet",
    precio: 300,
    disponible: true
}

console.table(productos)

//acceder a los valores
console.log(productos.nombre)
console.log(productos['nombre'])

//destructuring

const {nombre, disponible} = productos

console.log(nombre)
console.log(disponible)


//Object literal

const autenticado = true
const usuario = "jesus"

const usuarios = {
    autenticado ,
    usuario : usuario
}

console.log(usuarios)

